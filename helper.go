package main

import (
	"fmt"
	"skao.int/ska-ser-releases-notifier/pkg/models"
)

func getVersionStr(r models.Repository) string {
	if r.IsTag {
		return fmt.Sprintf(
			"• <%s|%s/%s> Tag: <%s|%s>\n",
			r.URL.String(),
			r.Owner,
			r.Name,
			r.URL.String()+"/releases/tag/"+r.Tag.Name,
			r.Tag.Name,
		)
	} else {
		return fmt.Sprintf(
			//"• <%s|%s/%s>\n\t◦ Tag: <%s|%s>\n\t◦ :clock5: %s\n",
			"• <%s|%s/%s> Tag: <%s|%s> :clock5: %s\n",
			r.URL.String(),
			r.Owner,
			r.Name,
			r.Release.URL.String(),
			r.Release.Name,
			r.Release.PublishedAt.Format("02 01 2006"),
		)
	}
}
