# ska-ser-releases-notifierfier

Receive Slack notifications if a new release is available on GitHub or Gitlab.

![screenshot.png](screenshot.png)

### Watching repositories

To watch repositories simply add them to the list of arguments `-r=github.com/kubernetes/kubernetes -r=gitlab.com/tezos/tezos` and so on.

### Variables

You can override the following variables in your PrivateRules.mak file:
```
GITHUB_AUTH_TOKEN=
GITLAB_AUTH_TOKEN=
# Slack channel webhook
SLACK_HOOK=
# ex: 1h, 1m, 30s
INTERVAL=1h
# ex: info, debug
LOG_LEVEL=debug
REPOSITORIE_CMD_ARGS=-r=github.com/kubernetes/kubernetes -r=github.com/kubernetes/minikube -r=gitlab.com/gitlab-org/gitlab-runner
DOCKER_IMG=ska-ser-releases-notifier
```

## Build image
```
# example
docker build -t ska-ser-releases-notifier .
```

### Deploying

1. Get a URL to send WebHooks to your Slack from https://api.slack.com/incoming-webhooks.
2. Get a token for scraping GitHub: [https://help.github.com/](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line).

#### Docker
After defining the enviroment variables and have built an image you can run the container with the following comand

```
make run
```
This will build the go app and start the container


That's it.

#### Credits

This project started from the [magmel48/github-releases-notifier](https://github.com/magmel48/github-releases-notifier) repository which is a fork from the original [justwatchcom/github-releases-notifier](https://github.com/justwatchcom/github-releases-notifier) repository.