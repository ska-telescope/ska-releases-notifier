package models

import (
	"net/url"
	"time"
)

type GithubQuery struct {
	Repository struct {
		ID          ID
		Name        String
		Description String
		URL         URI

		Releases struct {
			Edges []struct {
				Node struct {
					ID          ID
					Name        String
					Description String
					URL         URI
					PublishedAt DateTime
				}
			}
		} `graphql:"releases(first: 1)"`

		Refs struct {
			Nodes []struct {
				ID   ID
				Name String
			}
		} `graphql:"refs(refPrefix: \"refs/tags/\", first: 1, orderBy: { field: TAG_COMMIT_DATE, direction: DESC})"`
	} `graphql:"repository(owner: $owner, name: $name)"`
}

func (query GithubQuery) GetID() ID {
	return query.Repository.ID
}

func (query GithubQuery) GetName() String {
	return query.Repository.Name
}

func (query GithubQuery) GetDescription() String {
	return query.Repository.Description
}

func (query GithubQuery) GetURL() *url.URL {
	return query.Repository.URL.URL
}

func (query GithubQuery) GetReleasesCount() int {
	return len(query.Repository.Releases.Edges)
}

func (query GithubQuery) GetLatestReleaseID() ID {
	return query.Repository.Releases.Edges[0].Node.ID
}

func (query GithubQuery) GetLatestReleaseName() String {
	return query.Repository.Releases.Edges[0].Node.Name
}

func (query GithubQuery) GetLatestReleaseDescription() String {
	return query.Repository.Releases.Edges[0].Node.Description
}

func (query GithubQuery) GetLatestReleaseURL() *url.URL {
	return query.Repository.Releases.Edges[0].Node.URL.URL
}

func (query GithubQuery) GetLatestReleasePublishingDate() time.Time {
	return query.Repository.Releases.Edges[0].Node.PublishedAt.Time
}

func (query GithubQuery) GetTagsCount() int {
	return len(query.Repository.Refs.Nodes)
}

func (query GithubQuery) GetLastTagID() ID {
	return query.Repository.Refs.Nodes[0].ID
}

func (query GithubQuery) GetLastTagName() String {
	return query.Repository.Refs.Nodes[0].Name
}
