module skao.int/ska-ser-releases-notifier

go 1.21

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/go-kit/log v0.2.1
	github.com/joho/godotenv v1.5.1
	github.com/shurcooL/githubv4 v0.0.0-20230424031643-6cea62ecd5a9
	github.com/shurcooL/graphql v0.0.0-20220606043923-3cf50f8a0a29
	golang.org/x/oauth2 v0.9.0
)

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.11.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)