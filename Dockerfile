FROM golang:1.21 as builder

COPY . /go/src/skao.int/ska-ser-releases-notifier
WORKDIR /go/src/skao.int/ska-ser-releases-notifier

RUN make build

FROM alpine:3.18
RUN apk --no-cache add ca-certificates

COPY --from=builder /go/src/skao.int/ska-ser-releases-notifier /bin/
ENTRYPOINT [ "/bin/ska-ser-releases-notifier" ]
