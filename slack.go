package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"skao.int/ska-ser-releases-notifier/pkg/models"
)

// SlackSender has the hook to send slack notifications.
type SlackSender struct {
	Hook     string
	Username string
	Icon     string
}

type slackPayload struct {
	Username string `json:"username"`
	IconUrl  string `json:"icon_url"`
	Text     string `json:"text"`
}

func (s *SlackSender) SendMsg(msg string) error {

	payload := slackPayload{
		Username: s.Username,
		IconUrl:  s.Icon,
		Text:     msg,
	}

	return s.Send(payload)
}

// Send a notification with a formatted message build from the repository.
func (s *SlackSender) SendReleaseMsg(repository models.Repository) error {

	var payload slackPayload

	if repository.IsTag {
		// github tag
		payload = slackPayload{
			Username: s.Username,
			IconUrl:  s.Icon,
			Text: fmt.Sprintf(
				":mega: New release:\n• <%s|%s/%s>\n\t◦ Tag: <%s|%s>",
				repository.URL.String(),
				repository.Owner,
				repository.Name,
				repository.URL.String()+"/releases/tag/"+repository.Tag.Name,
				repository.Tag.Name,
			),
		}
	} else {
		// github and gilab release
		payload = slackPayload{
			Username: s.Username,
			IconUrl:  s.Icon,
			Text: fmt.Sprintf(
				":mega: New release:\n• <%s|%s/%s>\n\t◦ Tag: <%s|%s>\n\t◦ :clock5: %s",
				repository.URL.String(),
				repository.Owner,
				repository.Name,
				repository.Release.URL.String(),
				repository.Release.Name,
				repository.Release.PublishedAt.Format("02 01 2006"),
			),
		}
	}

	return s.Send(payload)
}

func (s *SlackSender) Send(payload slackPayload) error {

	payloadData, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, s.Hook, bytes.NewReader(payloadData))
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	req = req.WithContext(ctx)
	defer cancel()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("request didn't respond with 200 OK: %s, %s", resp.Status, body)
	}

	return nil
}
