package main

import (
	"os"
	"strings"
	"time"
	"log/slog"
	"path/filepath"

	"github.com/alexflint/go-arg"
	"github.com/joho/godotenv"
	models2 "skao.int/ska-ser-releases-notifier/pkg/models"
)

func main() {
	_ = godotenv.Load()

	c := models2.Config{
		Interval: time.Hour,
		LogLevel: "info",
	}
	arg.MustParse(&c)

	replacer := func(groups []string, a slog.Attr) slog.Attr {
		if a.Key == slog.SourceKey {
			source := a.Value.Any().(*slog.Source)
			source.File = filepath.Base(source.File)
		}
		return a
	}

	var loggingLevel = new(slog.LevelVar)
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: loggingLevel, AddSource: true, ReplaceAttr: replacer}))
	
	switch strings.ToLower(c.LogLevel) {
	case "debug":
		loggingLevel.Set(slog.LevelDebug)
	case "warn":
		loggingLevel.Set(slog.LevelWarn)
	case "error":
		loggingLevel.Set(slog.LevelError)
	default:
		loggingLevel.Set(slog.LevelInfo)
	}

	if len(c.Repositories) == 0 {
		logger.Error("no repositories to watch")
		os.Exit(1)
	}

	checker := &Checker{
		logger: logger,
		tokens: map[string]string{Github: c.GithubAuthToken, Gitlab: c.GitlabAuthToken},
	}

	strVersions := checker.GetVersions(c.Repositories)

	slack := SlackSender{Hook: c.Hook, Username: c.Username, Icon: c.Icon}

	msg := "\n:mega::mega: Release Notifier Started :mega::mega:\n\n"
	msg += strVersions
	msg += "\nWaiting for new releases :hourglass:"

	if err := slack.SendMsg(msg); err != nil {
		logger.Error("failed to send message to slack", "error", err,)		
	}

	releases := make(chan models2.Repository)
	go checker.Run(c.Interval, c.Repositories, releases)

	logger.Info("waiting for new releases")
	for repository := range releases {
		logger.Debug("got new release", "version", repository.Owner+"/"+repository.Name)
		if c.IgnoreNonstable && repository.Release.IsNonstable() {
			logger.Debug("not notifying about non-stable version", "version", repository.Release.Name)
			continue
		}

		if err := slack.SendReleaseMsg(repository); err != nil {
			logger.Error(
				"failed to send message to slack",
				"error", err,
			)				
			continue
		}
	}
}
