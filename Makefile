-include .make/base.mk
# include makefile targets for Oci images management
-include .make/oci.mk

GITHUB_AUTH_TOKEN ?=
GITLAB_AUTH_TOKEN ?=
SLACK_HOOK ?=
LOG_LEVEL ?= info
INTERVAL ?= 1h
REPOSITORIE_CMD_ARGS ?= -r=github.com/kubernetes/kubernetes -r=github.com/kubernetes/minikube -r=gitlab.com/gitlab-org/gitlab-runner
DOCKER_IMG ?= registry.gitlab.com/ska-telescope/sdi/ska-ser-releases-notifier/ska-ser-releases-notifier:0.1.0
IGNORE_NONSTABLE ?= false

 # include your own private variables for custom deployment configuration
-include PrivateRules.mak

DIST := dist
BIN := bin

EXECUTABLE := ska-ser-releases-notifier

PWD := $(shell pwd)
#VERSION := $(shell cat VERSION)
SHA := $(shell cat COMMIT 2>/dev/null || git rev-parse --short=8 HEAD)
DATE := $(shell date -u '+%FT%T%z')

#GOLDFLAGS += -X "main.version=$(VERSION)"
GOLDFLAGS += -X "main.date=$(DATE)"
GOLDFLAGS += -X "main.commit=$(SHA)"
GOLDFLAGS += -extldflags '-static'

GO := CGO_ENABLED=0 go

GOOS ?= linux #$(shell go version | cut -d' ' -f4 | cut -d'/' -f1)
GOARCH ?= amd64 #$(shell go version | cut -d' ' -f4 | cut -d'/' -f2)

PACKAGES ?= skao.int/ska-ser-releases-notifier skao.int/ska-ser-releases-notifier/pkg/models #$(shell go list ./... | grep -v /vendor/ | grep -v /tests)

TAGS ?= netgo

vars:
	@echo "\033[vars:\033[0m"
	@echo "DOCKER_IMG=$(DOCKER_IMG)"
	@echo "GITHUB_AUTH_TOKEN=$(GITHUB_AUTH_TOKEN)"
	@echo "GITLAB_AUTH_TOKEN=$(GITLAB_AUTH_TOKEN)"
	@echo "SLACK_HOOK=$(SLACK_HOOK)"
	@echo "LOG_LEVEL=$(LOG_LEVEL)"
	@echo "INTERVAL=$(INTERVAL)"
	@echo "REPOSITORIE_CMD_ARGS=$(REPOSITORIE_CMD_ARGS)"

.PHONY: all
all: clean test build

.PHONY: clean
clean:
	$(GO) clean -i ./...
	find . -type f -name "coverage.out" -delete

.PHONY: fmt
fmt:
	$(GO) fmt $(PACKAGES)

.PHONY: tests
tests: test vet lint errcheck megacheck

.PHONY: vet
vet:
	$(GO) vet $(PACKAGES)

.PHONY: lint
lint:
	@which golint > /dev/null; if [ $$? -ne 0 ]; then \
		$(GO) get -u github.com/golang/lint/golint; \
	fi
	STATUS=0; for PKG in $(PACKAGES); do golint -set_exit_status $$PKG || STATUS=1; done; exit $$STATUS

.PHONY: errcheck
errcheck:
	@which errcheck > /dev/null; if [ $$? -ne 0 ]; then \
		$(GO) get -u github.com/kisielk/errcheck; \
	fi
	STATUS=0; for PKG in $(PACKAGES); do errcheck $$PKG || STATUS=1; done; exit $$STATUS

.PHONY: megacheck
megacheck:
	@which megacheck > /dev/null; if [ $$? -ne 0  ]; then \
		$(GO) get -u honnef.co/go/tools/cmd/megacheck; \
	fi
	STATUS=0; for PKG in $(PACKAGES); do megacheck $$PKG || STATUS=1; done; exit $$STATUS

.PHONY: test
test:
	STATUS=0; for PKG in $(PACKAGES); do go test -cover -coverprofile $$GOPATH/src/$$PKG/coverage.out $$PKG || STATUS=1; done; exit $$STATUS

.PHONY: build
build: $(EXECUTABLE)-$(GOOS)-$(GOARCH)

$(EXECUTABLE)-$(GOOS)-$(GOARCH): $(wildcard *.go)
	$(GO) build -tags '$(TAGS)' -ldflags '-s -w $(GOLDFLAGS)' -o $(EXECUTABLE)

docker: 
	docker build -t $(DOCKER_IMG) .

run:
	docker run --rm -e GITHUB_AUTH_TOKEN=$(GITHUB_AUTH_TOKEN) -e GITLAB_AUTH_TOKEN=$(GITLAB_AUTH_TOKEN) -e SLACK_HOOK=$(SLACK_HOOK) -e INTERVAL=$(INTERVAL) -e LOG_LEVEL=$(LOG_LEVEL) -e IGNORE_NONSTABLE=$(IGNORE_NONSTABLE) $(DOCKER_IMG) $(REPOSITORIE_CMD_ARGS)
