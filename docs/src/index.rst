.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA RELEASES NOTIFIER
=====================

This project is an app that monitores github and giltlab software releases and notifies in a slack channel when a new release is available

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README

